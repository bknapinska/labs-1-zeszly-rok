﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Policjant : IPolicjant
    {
        public string Pracuj()
        {
            return "wypij kawe i zjedz paczka";
        }

        public string Aresztuj()
        {
            return "łap złodzieja";
        }
}
}
