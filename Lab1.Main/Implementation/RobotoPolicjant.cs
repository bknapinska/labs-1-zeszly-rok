﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class RobotoPolicjant : IPolicjant, IRobot
    { 
        public string Pracuj()
        {
            return "start";
        }
        string IPolicjant.Pracuj()
        {
            return "wypij kawe i zjedz paczka";
        }
        string IPolicjant.Aresztuj()
        {
            return "goń złodzieja";
        }
        string IMundurowy.Pracuj()
        {
            return "rozpocznij prace";
        }
        string IRobot.Pracuj()
        {
            return "włącz system";
        }
        string IRobot.Włącz()
        {
            return "on";
        }
       
    }
}
