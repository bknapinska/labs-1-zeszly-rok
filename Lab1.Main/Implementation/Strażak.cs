﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Strażak : IStrażak
    {
        public string Pracuj()
        {
            return "włóż mundur";
        }
        public string Ratuj()
        {
            return "gaś ogień";
        }
    }
}
