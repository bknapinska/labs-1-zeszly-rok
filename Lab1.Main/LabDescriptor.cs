﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMundurowy);
        
        public static Type ISub1 = typeof(IPolicjant);
        public static Type Impl1 = typeof(Policjant);
        
        public static Type ISub2 = typeof(IStrażak);
        public static Type Impl2 = typeof(Strażak);
        
        
        public static string baseMethod = "Pracuj";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Aresztuj";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Ratuj";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Kolekcja";
        public static string collectionConsumerMethod = "Zatrudnij";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3 = typeof(RobotoPolicjant);

        public static string otherCommonMethod = "Pracuj";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
