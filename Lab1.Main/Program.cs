﻿using System;
using System.Collections.Generic;
using Lab1.Implementation;
using Lab1.Contract;

namespace Lab1.Main
{
    public class Program
    {
        public ICollection<IMundurowy> Kolekcja()
        {
            return new List<IMundurowy>{new Policjant(), new Policjant(), new Strażak()};
        }
        public void Zatrudnij(ICollection<IMundurowy> k)
        {
            foreach (var i in k)
            {
                i.Pracuj();
            }
        }
        static void Main(string[] args)
        {
            var nowy = new RobotoPolicjant();
            Console.WriteLine(nowy.Pracuj());
            Console.WriteLine("Mundurowy: " + (nowy as IMundurowy).Pracuj());
            Console.WriteLine("Policjant: " + (nowy as IPolicjant).Pracuj());
            Console.WriteLine("Robot: " + (nowy as IRobot).Pracuj());
            Console.ReadKey();
            
        }
    }
}
