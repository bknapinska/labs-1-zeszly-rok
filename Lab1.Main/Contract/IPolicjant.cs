﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    interface IPolicjant : IMundurowy
    {
        string Pracuj();
        string Aresztuj();
    }
}
